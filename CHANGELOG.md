# Changelog

## [1.0]
### Added
- Polygon Boolean Ops ( UNION / INTERSECT / DIFFERENCE / XOR )
- Add polygon helper functions ( rotation, breakdown to line segments / points, et cetera )
- Add line segment helper functions ( orthogonal / parallel, scaling, angle, orientation )

For more information, see docs/V1.0.md
