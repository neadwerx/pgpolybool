## __cast_path_to_line

### Prototype:

LINE __cast_path_to_line( PATH )

### Description:

Casts the input PATH to a line by determining the best-fit line for the points in the path.

### Error Condition:

Emits an error and aborts the transaction on failure to allocate memory.
