## __cast_lseg_to_line

### Prototype:

LINE __cast_lseg_to_line( LSEG )

### Description:

Casts the input LSEG to a line by solving for the LSEGs line function

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
