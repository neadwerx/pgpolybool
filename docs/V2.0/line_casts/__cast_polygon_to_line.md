## __cast_polygon_to_line

### Prototype:

LINE __cast_polygon_to_line( POLYGON )

### Description:

Casts the input POLYGON to a line by determining the best fit line for the polygons points.

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
