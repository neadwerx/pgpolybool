## __cast_line_to_point

### Prototype:

POINT __cast_line_to_point( LINE )

### Description:

Casts the input LINE to a point by returning the y-intercept of the input LINE.

## Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
