# Point Casting

These functions cast various PostgreSQL geometric types to a POINT. These casts involve returning the centroid of the object that is being casted.
