## __cast_point_to_lseg

### Prototype:

LSEG __cast_point_to_lseg( POINT )

### Description:

Casts the input POINT to a line segment by forming a segment extending from the origin to the input POINT.

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
