## fn_get_polygon_line_segments

### Prototype:

LSEG[] fn_get_polygon_line_segments( POLYGON )

### Description:

Retuns an array of LSEGs which comprise the edges of the input POLYGON

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
