## fn_points_to_polygon

### Prototype:

POLYGON fn_points_to_polygon( VARIADIC POINT[] )

### Description:

Converts a POINT array to a POLYGON.

### Error Conditions:

Emits an error and aborts the trasaction on failure to allocate memory.
