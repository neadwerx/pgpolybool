## fn_get_polygon_area

### Prototype:

DOUBLE PRECISION fn_get_polygon_area( POLYGON )

### Description:

Computes the area of the input POLYGON using Gauss Formula (Shoelace Formula). This function does not return correct results for polygons that are self intersecting.

### Error Conditions:

None.
