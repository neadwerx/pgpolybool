## fn_get_points_ombb

### Prototype:

POINT[] fn_get_points_ombb( POLYGON )

### Description:

Returns the POINT array containing the points that form the oriented minimum bounding box for the input POLYGON

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
