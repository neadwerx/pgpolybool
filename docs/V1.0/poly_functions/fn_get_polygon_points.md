## fn_get_polygon_points

### Prototype:

POINT[] fn_get_polygon_points( POLYGON )

### Description:

Returns an array of points that comprise the input POLYGON

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
