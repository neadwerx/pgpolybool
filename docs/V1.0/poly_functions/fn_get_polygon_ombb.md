## fn_get_polygon_ombb

### Prototype:

POLYGON fn_get_polygon_ombb( POLYGON )

### Description:

Returns the POLYGON that is the oriented minimum bounding box for the input POLYGON.

### Error Conditions:

Emits an error and aborts the transaction on failure to allocate memory.
