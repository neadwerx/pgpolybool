## fn_get_lseg_angle

### Prototype:

DOUBLE PRECISION fn_get_lseg_angle( LSEG, LSEG )

### Description:

Get the angle (in radians) between the supplied line segments.

### Error Conditions:

This function will return NULL on NULL input.
