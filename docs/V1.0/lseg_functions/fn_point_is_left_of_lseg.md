## fn_point_is_left_of_lseg

### Prototype:

BOOLEAN fn_point_is_left_of_lseg( LSEG, POINT, POINT )

### Description:

Determines if the second POINT argument is to the left of the segment LSEG, where the third POINT argument indicates the front of the segment

### Error Conditions:

Returns NULL on NULL input.
