# fn_xor_polygons

## Prototype:

POLYGON fn_xor_polygons( POLYGON[] )
POLYGON fn_xor_polygons( POLYGON, POLYGON )

## Descriptions

Compute the exclusive-or of two or more input polygons.

## Error Conditions:

This function will emit an error and abort the transaction if there is a failure to allocate memory.
