## test/fuzzer.pl

### Description:

This is an input fuzzing script for the pgpolybool boolean op functions. This script is included in the `make check` tests.

When called from the command line, the script can be used to benchmark the runtimes of the polybool functions.
